#!/usr/bin/env bash

# Specify old and new disks
oldDisk=""
newDisk=""
partition="2"   # By default Proxmox uses partition 2 for ZFS
pool="rpool"    # By default the boot pool is called rpool

# Take the old drive offline
zpool offline $pool $oldDisk$partition

# This is the appropriate partitioning for Proxmox's rpool as done by the installer
# https://forum.proxmox.com/threads/autoexpand-on-rpool-zfs.30512/
sgdisk -a1 -n1:34:2047 -t1:EF02 -n9:-8M:0 -t9:BF07 -n2:2048:0 -t2:BF01 -c 2:zfs $newDisk

# Make sure GUIDs don't overlap
sgdisk --randomize-guids $newDisk

# install grub
grub-install $newDisk

# Replace the old disk with the new one in rpool
zpool replace $pool $oldDisk$partition $newDisk$partition

sleep 5
zpool status $pool

printf '%s\n' "Let the resilvering finish on $pool and then don't forget to run 'update-grub'"

exit 0
