This is a group of scripts to make certain management tasks on Proxmox easier to deal with.

For example:

* `running` - outputs a list of VMs/CTs that are currently running
* `diskids` - outputs the disk-ids of all disks connected to host in a pretty format
* `proxpower` - shutsdown or reboots host system by powering off VMs/CTs in a predefined manner. This one's good to have if you're running a NAS VM that provides storage back to the system.
