#!/usr/bin/env bash

# Prune Old PVE Snapshots, keeping most recent X snaps
keep=10         # Set number of snapshots you'd like to keep
running_cmd=running

print_usage() {
  printf '%s\n' "This command allows bulk deletion of snapshots on Proxmox. Specify VMID with -i; specify number of recent snapshots to keep if -k"
}

# Exit if 'running' command isn't present
if [[ $(command -v running | wc -l) == 0 ]]; then
    echo "The command 'running' could not be found on this system; please install it before continuing"
    exit 1
fi

while getopts 'k:i:h' flag; do
    case "${flag}" in 
        k) keep=${OPTARG}  ;;
        i) vm_id=${OPTARG} ;;
        h) print_usage; exit 2 ;;
        *) print_usage; exit 2 ;;
    esac
done

# Ask for the VM/LXC ID number
if [[ -z $vm_id ]]; then
    read -p "Enter the id number of the VM or container you'd like to prune: " vm_id
fi

# vm_id=401       # Enter VMID of the VM
# vm_type=qemu    # Choose qemu or lxc
# vm_node=lympus  # Enter PVE node name
vm_type=$($running_cmd | grep ${vm_id} | awk -F/ '{gsub(" ", "", $1); print $1}')
vm_node=$($running_cmd | grep ${vm_id} | awk '{print $3}')
del_snap_cmd="pvesh delete /nodes/${vm_node}/${vm_type}/${vm_id}/snapshot"

# Get all snapshots
get_snaps() {
    pvesh get /nodes/${vm_node}/${vm_type}/${vm_id}/snapshot --noborder | awk '{print $3 " " $4 "   " $1}' | grep "^[0-9]" | sort -h
}

old_snaps() {
    get_snaps | head -n -${keep}
}

current_snaps() {
    get_snaps | tail -n ${keep}
}

delete_old_snaps() {
    old_snaps | awk '{print $3}' | while read -r line; do echo "$line" & ${del_snap_cmd}/$line || qm unlock ${vm_id}; done
}

echo "The following $(old_snaps | wc -l) snapshots on VMID ${vm_id} are marked as old:"
old_snaps

echo " "

echo "The following $(current_snaps | wc -l) will be kept on VMID ${vm_id}:"
current_snaps

read -r -p 'Do you want to continue? (y|N)' choice
    case "$choice" in
      y|Y) delete_old_snaps;;
      *) exit 1;;
    esac
