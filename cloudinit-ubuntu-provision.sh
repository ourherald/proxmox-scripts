#!/usr/bin/env bash

# Script to provision cloud-init template for ubuntu

LTS=jammy   # Set code name for LTS version you want
STORE=virtuals-gilead
VMID=9000
packages=qemu-guest-agent,curl,git,tmux,vim,htop

# Download base image
cd /mnt/pve/${STORE}/template/iso
curl -LO https://cloud-images.ubuntu.com/${LTS}/current/${LTS}-server-cloudimg-amd64.img

# Install components
virt-customize -a ${LTS}-server-cloudimg-amd64.img --install [${packages}]

# Install SSH key
virt-customize -a ${LTS}-server-cloudimg-amd64.img --ssh-inject root:file:/root/.ssh/authorized_keys


qm create ${VMID} --name "ubuntu-${LTS}-cloudinit-template" --memory 2048 --cores 2 --net0 virtio,bridge=vmbr0
qm importdisk ${VMID} ${LTS}-server-cloudimg-amd64.img ${STORE}
qm set ${VMID} --scsihw virtio-scsi-pci --scsi0 ${STORE}:${VMID}/vm-${VMID}-disk-0
qm set ${VMID} --boot c --bootdisk scsi0
qm set ${VMID} --ide2 ${STORE}:cloudinit
qm set ${VMID} --serial0 socket --vga serial0
qm set ${VMID} --agent enabled=1
qm set ${VMID} --ipconfig0 ip=dhcp

qm template ${VMID}
