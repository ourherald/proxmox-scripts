#!/usr/bin/env bash

ctid=$1

pve_local=$(hostname -s)
target_host=$(running | grep -w "lxc/${ctid}" | awk '{print $3}')

if [[ ${pve_local} == ${target_host} ]]; then
    pct enter ${ctid}
else
    ssh -t ${target_host} "pct enter ${ctid}; exec bash"
fi

